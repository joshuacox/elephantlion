# ElephantLion

All work done in [Inkscape](https://inkscape.org/) with a final
reduction in size performed in the [Gimp](https://www.gimp.org/).

## sources

### fonts

https://dejavu-fonts.github.io/

### Wikimedia
Under licenses that permit modification even for commercial use.

### Elephant mouse om sanskrit

https://commons.wikimedia.org/wiki/File:Ganesha_elephant_mouse_om_sanskrit.svg
https://upload.wikimedia.org/wikipedia/commons/9/93/Ganesha_elephant_mouse_om_sanskrit.svg

original author:
https://hi.wikipedia.org/wiki/%E0%A4%B8%E0%A4%A6%E0%A4%B8%E0%A5%8D%E0%A4%AF:Guy_vandegrift

### Lion and Sun Emblem of Iran Pahlavi

https://commons.wikimedia.org/wiki/File:Lion_and_Sun_Emblem_of_Iran_(Pahlavi).svg
https://upload.wikimedia.org/wikipedia/commons/3/3b/Lion_and_Sun_Emblem_of_Iran_%28Pahlavi%29.svg

### Conservative Elephant

https://commons.wikimedia.org/wiki/File:Conservative_Elephant.svg
https://upload.wikimedia.org/wikipedia/commons/4/4b/Conservative_Elephant.svg

### Republican Elephant

https://commons.wikimedia.org/wiki/File:Republicanlogo.svg
https://commons.wikimedia.org/wiki/File:Republicanlogo.svg
